
#Evaluator 
##Table of contents

*	Introduction
*	Authorisation
*	Exams
*	Help
*	Methods
*	Usage
- - - - - 
##Introduction
---
Evaluator is an api made for C++ compiling and also a libary of exams that users can take to improve their coding skills. The exams are immediately evaluated and scored. User gets instant feedback on his coding skills after he finishes the exam.
##Authorisation
--- 
```
POST/auth/
``` 
##Exams 
---
```
GET/exams/
```
_This gives a list of all exams._

```
{id: x}/
```
_This gives exam with id "x"._
```
{id: x}/results
``` 
_This gives results for exam with id "x"._

##Help
---
```
POST/help/
```
##Methods
---
*	 GET - used for retrieving lists and individual objects
*	POST - used for creating new objects
