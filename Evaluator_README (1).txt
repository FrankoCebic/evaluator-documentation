
#Evaluator 
##Table of contents

*	Introduction
*	Authorisation
*	Exams
*	Help
*	Methods
*	Usage
- - - - - 
##Introduction
---
Evaluator is an api made for C++ compiling and also a libary of exams that users can take to improve their coding skills. The exams are immediately evaluated and scored. User gets instant feedback on his coding skills after he finishes the exam.
##Authorisation
--- 
_Route_
```
POST/auth/
``` 
_Request_
```
{	"email": string ,
	"password": string
	}
```
_Response_ 
```
{ 	"token": string
	}
```
##Exams 
---
_Route_
```
GET/exams/
```
_Request_
```
{	"exams": []
	}
```
_Response_ 
```
{	"exams": [
			exam{id},
			exam{id},
			exam{id}
			...
			]
		}
```
_This gives a list of all exams._

---
_Route_ 
```
GET/auth/exams/{id:x}/
```
_Request_
```
{	"exams-tasks{id}": []
	}
```
_Response_
```
{	"exam-tasks": [
		task{id},
		task{id},
		task{id},
		...
		]
	}
```
_This gives exam with id "x"._

---
_Route_
```
GET/auth/exams/{id: x}/results
```
_Request_
```
{	"exam-results": []
	}
```
_Response_ 
```
{	"exam-results": [
		result{id},
		result{id},
		result{id},
		...
		]
	}

```
_This gives results for exam with id "x"._

##Help
---
_Route_
```
GET/help/
```
_Request_
```
{	"help": []
	}
```
_Response_
```
{	"help": string
	}
```
##Methods
---
*	 GET - used for retrieving lists and individual objects
*	POST - used for creating new objects
##Usage
---
_Route_
```
POST/exam/{id}/task/{id}
```
_Request_
```
{	"email": "string", 
	"code": "string"
	}
```
_Response_
```
{	"valid": int, 
	"max": int
	}
```